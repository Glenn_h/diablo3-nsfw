<%@ include file="/jsp/navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript" src ="jquery-2.2.2.min.js">
</script>

<script type="text/javascript">
${"#menu"}
</script>
<html>
<body>

	<form action="accounts">
		<div class="jumbotron">
			<a class="thumbnail" id="accountchoose" onclick="this.form.submit()"
				href="?name=Evilprince-2911&hero=3127572">Glenn</a>
			<a class="thumbnail" id="accountchoose" onclick="this.form.submit()"
				href="?name=Tokki-2720&hero=4211348">Kevin</a>
			<a class="thumbnail" id="accountchoose" onclick="this.form.submit()"
				href="?name=Skeletik-2631&hero=36768491">Quentin</a>
			<a class="thumbnail" id="accountchoose" onclick="this.form.submit()"
				href="?name=TiJoSsS-2442&hero=7089713">Joss</a>
			<a class="thumbnail" id="accountchoose" onclick="this.form.submit()"
				href="?name=Thanos-2850&hero=4790564">Jean</a>
			<a class="thumbnail" id="accountchoose" onclick="this.form.submit()"
				href="?name=Tosh-2386&hero=7818466">Youen</a>
			<a class="thumbnail" id="accountchoose" onclick="this.form.submit()"
				href="?name=shaddval-2979&hero=20271980">Val</a>
			<a class="thumbnail" id="accountchoose" onclick="this.form.submit()"
				href="?name=Gu%C3%A9na-2715&hero=11470167">Gu�na</a>

			<br />
			<br />
			
			<div class="btn-group">
				<c:forEach items="${account.heroes}" var="hero">
						<a class="thumbnail" id="characterschoose"
							onclick="this.form.submit()"
							href="?name=${account.battleTag}&hero=${hero.id}">${hero.classe}
							- ${hero.name}</a>
				</c:forEach>
			</div>
			
			<c:if test="${item != null}">
				<div class="well" id="feuilleprofil">
					<a class="thumbnail" id="itemsthumb${item.shoulders.displayColor}"
						href="http://eu.battle.net/d3/fr/${item.shoulders.tooltipParams}">
						<img
							src="http://media.blizzard.com/d3/icons/items/large/${item.shoulders.icon}.png"
							onclick="return false"></img>
					</a>
					<a class="thumbnail" id="itemsthumb${item.head.displayColor}"
						href="http://eu.battle.net/d3/fr/${item.head.tooltipParams}">
						<img
							src="http://media.blizzard.com/d3/icons/items/large/${item.head.icon}.png"
							onclick="return false"></img>
					</a>
					<a class="thumbnail" id="itemsthumb${item.neck.displayColor}"
						href="http://eu.battle.net/d3/fr/${item.neck.tooltipParams}">
						<img
							src="http://media.blizzard.com/d3/icons/items/large/${item.neck.icon}.png"
							onclick="return false"></img>
					</a>
					<div class="clear"></div>
					<a class="thumbnail" id="itemsthumb${item.hands.displayColor}"
						href="http://eu.battle.net/d3/fr/${item.hands.tooltipParams}">
						<img
							src="http://media.blizzard.com/d3/icons/items/large/${item.hands.icon}.png"
							onclick="return false"></img>
					</a>
					<a class="thumbnail" id="itemsthumb${item.torso.displayColor}"
						href="http://eu.battle.net/d3/fr/${item.torso.tooltipParams}">
						<img
							src="http://media.blizzard.com/d3/icons/items/large/${item.torso.icon}.png"
							onclick="return false"></img>
					</a>
					<a class="thumbnail" id="itemsthumb${item.bracers.displayColor}"
						href="http://eu.battle.net/d3/fr/${item.bracers.tooltipParams}">
						<img
							src="http://media.blizzard.com/d3/icons/items/large/${item.bracers.icon}.png"
							onclick="return false"></img>
					</a>
					<div class="clear"></div>

					<a class="thumbnail" id="itemsthumb${item.leftFinger.displayColor}"
						href="http://eu.battle.net/d3/fr/${item.leftFinger.tooltipParams}">
						<img
							src="http://media.blizzard.com/d3/icons/items/large/${item.leftFinger.icon}.png"
							onclick="return false"></img>
					</a>
					<a class="thumbnail" id="itemsthumb${item.waist.displayColor}"
						href="http://eu.battle.net/d3/fr/${item.waist.tooltipParams}">
						<img
							src="http://media.blizzard.com/d3/icons/items/large/${item.waist.icon}.png"
							onclick="return false"></img>
					</a>

					<a class="thumbnail" id="itemsthumb${item.rightFinger.displayColor}"
						href="http://eu.battle.net/d3/fr/${item.rightFinger.tooltipParams}">
						<img
							src="http://media.blizzard.com/d3/icons/items/large/${item.rightFinger.icon}.png"
							onclick="return false"></img>
					</a>

					<div class="clear"></div>
					<a class="thumbnail" id="itemsthumb${item.mainHand.displayColor}"
						href="http://eu.battle.net/d3/fr/${item.mainHand.tooltipParams}">
						<img
							src="http://media.blizzard.com/d3/icons/items/large/${item.mainHand.icon}.png"
							onclick="return false"></img>
					</a>
					<a class="thumbnail" id="itemsthumb${item.legs.displayColor}"
						href="http://eu.battle.net/d3/fr/${item.legs.tooltipParams}">
						<img
							src="http://media.blizzard.com/d3/icons/items/large/${item.legs.icon}.png"
							onclick="return false"></img>
					</a>
					<c:if test="${item.offHand != null}">
						<a class="thumbnail" id="itemsthumb${item.offHand.displayColor}"
							href="http://eu.battle.net/d3/fr/${item.offHand.tooltipParams}">
							<img
								src="http://media.blizzard.com/d3/icons/items/large/${item.offHand.icon}.png"
								onclick="return false"></img>
						</a>
					</c:if>
					<div class="clear"></div>
					<a class="thumbnail" id="itemsthumb${item.feet.displayColor}" style="margin-left:75px"
						href="http://eu.battle.net/d3/fr/${item.feet.tooltipParams}">
						<img
							src="http://media.blizzard.com/d3/icons/items/large/${item.feet.icon}.png"
							onclick="return false"></img>
					</a>

				</div>
			</c:if>

			<script>
				var b = Bnet.D3.Tooltips;
				b.registerDataOld = b.registerData;
				b.registerData = function(data) {
					var c = document.body.children, s = c[c.length - 1].src;
					data.params.key = s.substr(0, s.indexOf('?')).substr(
							s.lastIndexOf('/') + 1);
					this.registerDataOld(data);
				}
			</script>
		</div>
	</form>
</body>
</html>
