package com.diablo3.classes;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Hero {
	@JsonProperty("class")
	public String classe;
	public boolean dead;
	public Number gender;
	public boolean hardcore;
	public Number id;
	public Items items;
	public Kills kills;
	@JsonProperty("last-updated")
	public Number lastupdated;
	public Number level;
	public String name;
	public Number paragonLevel;
	public Number seasonCreated;
	public boolean seasonal;
	public Skills skills;
	public Stats stats;

	public String getClasse() {
		switch (classe) {
		case "wizard":
			return "Sorcier";
		case "monk":
			return "Moine";
		case "barbarian":
			return "Barbare";
		case "demon-hunter":
			return "Chasseur de d�mons";
		case "crusader":
			return "Crois�";
		case "witch-doctor":
			return "F�ticheur";
		default:
			return classe;
		}
	}

	public void setClasse(String classe) {
		this.classe = classe;
	}

	public boolean isDead() {
		return dead;
	}

	public void setDead(boolean dead) {
		this.dead = dead;
	}

	public Number getGender() {
		return gender;
	}

	public void setGender(Number gender) {
		this.gender = gender;
	}

	public boolean isHardcore() {
		return hardcore;
	}

	public void setHardcore(boolean hardcore) {
		this.hardcore = hardcore;
	}

	public Number getId() {
		return id;
	}

	public void setId(Number id) {
		this.id = id;
	}

	public Items getItems() {
		return items;
	}

	public void setItems(Items items) {
		this.items = items;
	}

	public Kills getKills() {
		return kills;
	}

	public void setKills(Kills kills) {
		this.kills = kills;
	}

	public Number getLastupdated() {
		return lastupdated;
	}

	public void setLastupdated(Number lastupdated) {
		this.lastupdated = lastupdated;
	}

	public Number getLevel() {
		return level;
	}

	public void setLevel(Number level) {
		this.level = level;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Number getParagonLevel() {
		return paragonLevel;
	}

	public void setParagonLevel(Number paragonLevel) {
		this.paragonLevel = paragonLevel;
	}

	public Number getSeasonCreated() {
		return seasonCreated;
	}

	public void setSeasonCreated(Number seasonCreated) {
		this.seasonCreated = seasonCreated;
	}

	public boolean isSeasonal() {
		return seasonal;
	}

	public void setSeasonal(boolean seasonal) {
		this.seasonal = seasonal;
	}

	public Skills getSkills() {
		return skills;
	}

	public void setSkills(Skills skills) {
		this.skills = skills;
	}

	public Stats getStats() {
		return stats;
	}

	public void setStats(Stats stats) {
		this.stats = stats;
	}
}
