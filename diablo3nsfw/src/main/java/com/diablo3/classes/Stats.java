package com.diablo3.classes;

public class Stats {
	public String blockChance;
	public String armor;
	public String damageReduction;
	public String attackSpeed;
	public String dexterity;
	public String physicalResist;
	public String life;
	public String arcaneResist;
	public String goldFind;
	public String critDamage;
	public String poisonResist;
	public String thorns;
	public String toughness;
	public String blockAmountMax;
	public String healing;
	public String intelligence;
	public String magicFind;
	public String lightningResist;
	public String strength;
	public String blockAmountMin;
	public String fireResist;
	public String damage;
	public String vitality;
	public String critChance;
	public String lifePerKill;
	public String lifeOnHit;
	public String damageIncrease;
	public String lifeSteal;
	public String coldResist;
	public String primaryResource;
	public String secondaryResource;
}
