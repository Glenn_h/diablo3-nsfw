
package com.diablo3.classes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

public class Account {

    @JsonProperty("battleTag")
    private String battleTag;
    @JsonProperty("paragonLevel")
    private Integer paragonLevel;
    @JsonProperty("paragonLevelHardcore")
    private Integer paragonLevelHardcore;
    @JsonProperty("paragonLevelSeason")
    private Integer paragonLevelSeason;
    @JsonProperty("paragonLevelSeasonHardcore")
    private Integer paragonLevelSeasonHardcore;
    @JsonProperty("heroes")
    private List<Hero> heroes = new ArrayList<Hero>();
    @JsonProperty("lastHeroPlayed")
    private Integer lastHeroPlayed;
    @JsonProperty("lastUpdated")
    private Integer lastUpdated;
    @JsonProperty("kills")
    private Kills kills;
    @JsonProperty("highestHardcoreLevel")
    private Integer highestHardcoreLevel;
    @JsonProperty("timePlayed")
    private Object timePlayed;
    @JsonProperty("progression")
    private Object progression;
    @JsonProperty("fallenHeroes")
    private List<Object> fallenHeroes = new ArrayList<Object>();
    @JsonProperty("blacksmith")
    private Object blacksmith;
    @JsonProperty("jeweler")
    private Object jeweler;
    @JsonProperty("mystic")
    private Object mystic;
    @JsonProperty("blacksmithHardcore")
    private Object blacksmithHardcore;
    @JsonProperty("jewelerHardcore")
    private Object jewelerHardcore;
    @JsonProperty("mysticHardcore")
    private Object mysticHardcore;
    @JsonProperty("blacksmithSeason")
    private Object blacksmithSeason;
    @JsonProperty("jewelerSeason")
    private Object jewelerSeason;
    @JsonProperty("mysticSeason")
    private Object mysticSeason;
    @JsonProperty("blacksmithSeasonHardcore")
    private Object blacksmithSeasonHardcore;
    @JsonProperty("jewelerSeasonHardcore")
    private Object jewelerSeasonHardcore;
    @JsonProperty("mysticSeasonHardcore")
    private Object mysticSeasonHardcore;
    @JsonProperty("seasonalProfiles")
    private Object seasonalProfiles;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("battleTag")
    public String getBattleTag() {
        return battleTag.replace('#', '-').replace("�", "%C3%A9");
    }

    @JsonProperty("battleTag")
    public void setBattleTag(String battleTag) {
        this.battleTag = battleTag;
    }

    @JsonProperty("paragonLevel")
    public Integer getParagonLevel() {
        return paragonLevel;
    }

    @JsonProperty("paragonLevel")
    public void setParagonLevel(Integer paragonLevel) {
        this.paragonLevel = paragonLevel;
    }

    @JsonProperty("paragonLevelHardcore")
    public Integer getParagonLevelHardcore() {
        return paragonLevelHardcore;
    }

    @JsonProperty("paragonLevelHardcore")
    public void setParagonLevelHardcore(Integer paragonLevelHardcore) {
        this.paragonLevelHardcore = paragonLevelHardcore;
    }

    @JsonProperty("paragonLevelSeason")
    public Integer getParagonLevelSeason() {
        return paragonLevelSeason;
    }

    @JsonProperty("paragonLevelSeason")
    public void setParagonLevelSeason(Integer paragonLevelSeason) {
        this.paragonLevelSeason = paragonLevelSeason;
    }

    @JsonProperty("paragonLevelSeasonHardcore")
    public Integer getParagonLevelSeasonHardcore() {
        return paragonLevelSeasonHardcore;
    }

    @JsonProperty("paragonLevelSeasonHardcore")
    public void setParagonLevelSeasonHardcore(Integer paragonLevelSeasonHardcore) {
        this.paragonLevelSeasonHardcore = paragonLevelSeasonHardcore;
    }

    @JsonProperty("heroes")
    public List<Hero> getHeroes() {
        return heroes;
    }

    @JsonProperty("heroes")
    public void setHeroes(List<Hero> heroes) {
        this.heroes = heroes;
    }

    @JsonProperty("lastHeroPlayed")
    public Integer getLastHeroPlayed() {
        return lastHeroPlayed;
    }

    @JsonProperty("lastHeroPlayed")
    public void setLastHeroPlayed(Integer lastHeroPlayed) {
        this.lastHeroPlayed = lastHeroPlayed;
    }

    @JsonProperty("lastUpdated")
    public Integer getLastUpdated() {
        return lastUpdated;
    }

    @JsonProperty("lastUpdated")
    public void setLastUpdated(Integer lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @JsonProperty("kills")
    public Kills getKills() {
        return kills;
    }

    @JsonProperty("kills")
    public void setKills(Kills kills) {
        this.kills = kills;
    }

    @JsonProperty("highestHardcoreLevel")
    public Integer getHighestHardcoreLevel() {
        return highestHardcoreLevel;
    }

    @JsonProperty("highestHardcoreLevel")
    public void setHighestHardcoreLevel(Integer highestHardcoreLevel) {
        this.highestHardcoreLevel = highestHardcoreLevel;
    }

    @JsonProperty("timePlayed")
    public Object getTimePlayed() {
        return timePlayed;
    }

    @JsonProperty("timePlayed")
    public void setTimePlayed(Object timePlayed) {
        this.timePlayed = timePlayed;
    }

    @JsonProperty("progression")
    public Object getProgression() {
        return progression;
    }

    @JsonProperty("progression")
    public void setProgression(Object progression) {
        this.progression = progression;
    }

    @JsonProperty("fallenHeroes")
    public List<Object> getFallenHeroes() {
        return fallenHeroes;
    }

    @JsonProperty("fallenHeroes")
    public void setFallenHeroes(List<Object> fallenHeroes) {
        this.fallenHeroes = fallenHeroes;
    }

    @JsonProperty("blacksmith")
    public Object getBlacksmith() {
        return blacksmith;
    }

    @JsonProperty("blacksmith")
    public void setBlacksmith(Object blacksmith) {
        this.blacksmith = blacksmith;
    }

    @JsonProperty("jeweler")
    public Object getJeweler() {
        return jeweler;
    }

    @JsonProperty("jeweler")
    public void setJeweler(Object jeweler) {
        this.jeweler = jeweler;
    }

    @JsonProperty("mystic")
    public Object getMystic() {
        return mystic;
    }

    @JsonProperty("mystic")
    public void setMystic(Object mystic) {
        this.mystic = mystic;
    }

    @JsonProperty("blacksmithHardcore")
    public Object getBlacksmithHardcore() {
        return blacksmithHardcore;
    }

    @JsonProperty("blacksmithHardcore")
    public void setBlacksmithHardcore(Object blacksmithHardcore) {
        this.blacksmithHardcore = blacksmithHardcore;
    }

    @JsonProperty("jewelerHardcore")
    public Object getJewelerHardcore() {
        return jewelerHardcore;
    }

    @JsonProperty("jewelerHardcore")
    public void setJewelerHardcore(Object jewelerHardcore) {
        this.jewelerHardcore = jewelerHardcore;
    }

    @JsonProperty("mysticHardcore")
    public Object getMysticHardcore() {
        return mysticHardcore;
    }

    @JsonProperty("mysticHardcore")
    public void setMysticHardcore(Object mysticHardcore) {
        this.mysticHardcore = mysticHardcore;
    }

    @JsonProperty("blacksmithSeason")
    public Object getBlacksmithSeason() {
        return blacksmithSeason;
    }

    @JsonProperty("blacksmithSeason")
    public void setBlacksmithSeason(Object blacksmithSeason) {
        this.blacksmithSeason = blacksmithSeason;
    }

    @JsonProperty("jewelerSeason")
    public Object getJewelerSeason() {
        return jewelerSeason;
    }

    @JsonProperty("jewelerSeason")
    public void setJewelerSeason(Object jewelerSeason) {
        this.jewelerSeason = jewelerSeason;
    }

    @JsonProperty("mysticSeason")
    public Object getMysticSeason() {
        return mysticSeason;
    }

    @JsonProperty("mysticSeason")
    public void setMysticSeason(Object mysticSeason) {
        this.mysticSeason = mysticSeason;
    }

    @JsonProperty("blacksmithSeasonHardcore")
    public Object getBlacksmithSeasonHardcore() {
        return blacksmithSeasonHardcore;
    }

    @JsonProperty("blacksmithSeasonHardcore")
    public void setBlacksmithSeasonHardcore(Object blacksmithSeasonHardcore) {
        this.blacksmithSeasonHardcore = blacksmithSeasonHardcore;
    }

    @JsonProperty("jewelerSeasonHardcore")
    public Object getJewelerSeasonHardcore() {
        return jewelerSeasonHardcore;
    }

    @JsonProperty("jewelerSeasonHardcore")
    public void setJewelerSeasonHardcore(Object jewelerSeasonHardcore) {
        this.jewelerSeasonHardcore = jewelerSeasonHardcore;
    }

    @JsonProperty("mysticSeasonHardcore")
    public Object getMysticSeasonHardcore() {
        return mysticSeasonHardcore;
    }

    @JsonProperty("mysticSeasonHardcore")
    public void setMysticSeasonHardcore(Object mysticSeasonHardcore) {
        this.mysticSeasonHardcore = mysticSeasonHardcore;
    }

    @JsonProperty("seasonalProfiles")
    public Object getSeasonalProfiles() {
        return seasonalProfiles;
    }

    @JsonProperty("seasonalProfiles")
    public void setSeasonalProfiles(Object seasonalProfiles) {
        this.seasonalProfiles = seasonalProfiles;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
