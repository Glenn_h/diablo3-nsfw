package com.diablo3.classes;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Skill {
	public String slug;
	public String name;
	public String icon;
	public Integer level;
	public String tooltipUrl;
	public String description;
	public String flavor;
	public String skillCalcId;
}
