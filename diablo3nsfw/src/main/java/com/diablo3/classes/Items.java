package com.diablo3.classes;

public class Items {
	private Item torso;
	private Item neck;
	private Item mainHand;
	private Item leftFinger;
	private Item feet;
	private Item rightFinger;
	private Item waist;
	private Item shoulders;
	private Item hands;
	private Item bracers;
	private Item legs;
	private Item head;
	private Item offHand;
	
	public Item getTorso() {
		return torso;
	}
	public Item getNeck() {
		return neck;
	}
	public Item getMainHand() {
		return mainHand;
	}
	public Item getLeftFinger() {
		return leftFinger;
	}
	public Item getFeet() {
		return feet;
	}
	public Item getRightFinger() {
		return rightFinger;
	}
	public Item getWaist() {
		return waist;
	}
	public Item getShoulders() {
		return shoulders;
	}
	public Item getHands() {
		return hands;
	}
	public Item getBracers() {
		return bracers;
	}
	public Item getLegs() {
		return legs;
	}
	public Item getHead() {
		return head;
	}
	public Item getOffHand() {
		return offHand;
	}
}
