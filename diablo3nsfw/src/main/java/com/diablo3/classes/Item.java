package com.diablo3.classes;

public class Item {
	private String id;
	private String name;
	private String icon;
	private String displayColor;
	private String tooltipParams;
	private Item transmogItem;
	private Object craftedBy;
	private Object recipe;
	
	public String getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public String getIcon() {
		return icon;
	}
	public String getDisplayColor() {
		return displayColor;
	}
	public String getTooltipParams() {
		return tooltipParams;
	}
	public Item getTransmogItem() {
		return transmogItem;
	}
	public Object getCraftedBy() {
		return craftedBy;
	}
	public Object getRecipe() {
		return recipe;
	}
	public void setRecipe(Object recipe) {
		this.recipe = recipe;
	}
}
