package com.diablo3.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;

import com.diablo3.classes.Account;
import com.diablo3.classes.Hero;

public class Accounts_page extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String URL_MAPPING = "/accounts";
	public static final String JSP_MAPPING = "/jsp/accounts.jsp";
	private static String URL = "https://eu.api.battle.net/d3/profile/";
	private static String APIKEY = "tmbv392ctg4snu9zkn67wnc6ft7rrwmx";

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		java.net.URL urlHero = new java.net.URL(URL + "/"
				+ request.getParameter("name") + "/hero/"
				+ request.getParameter("hero") + "?locale=fr_FR&apikey="
				+ APIKEY);
		java.net.URL urlAccount = new java.net.URL(URL + "/"
				+ request.getParameter("name") + "/?locale=fr_FR&apikey="
				+ APIKEY);
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			Hero hero = mapper.readValue(urlHero, Hero.class);
			Account account = mapper.readValue(urlAccount,Account.class);
			
	
			request.setAttribute("item", hero.items);
			request.setAttribute("account", account);
		} catch (Exception e) {
			e.printStackTrace();
		}

		RequestDispatcher d = request.getRequestDispatcher(JSP_MAPPING);

		d.forward(request, response);
	}
}
